import { Component } from "react";
import dice from "../assets/images/dice.png";
import dice1 from "../assets/images/1.png";
import dice2 from "../assets/images/2.png";
import dice3 from "../assets/images/3.png";
import dice4 from "../assets/images/4.png";
import dice5 from "../assets/images/5.png";
import dice6 from "../assets/images/6.png";

class Dice extends Component {
    constructor(props) {
        super(props)

        this.state = {
            dice: 0
        }
    }

    throwHandler = () => {
        let newDice = Math.floor(Math.random() * 6 + 1);
        this.setState({
            dice: newDice
        })
    }
    render() {
        return (
            <div>
                <button onClick={this.throwHandler}>Ném</button><br /><br />
                {(() => {



                    switch (this.state.dice) {

                        case 0:

                            return (

                                <img src={dice} width="100" />

                            )
                        case 1:

                            return (

                                <img src={dice1} width="100" />

                            )
                        case 2:

                            return (

                                <img src={dice2} width="100" />

                            )
                        case 3:

                            return (

                                <img src={dice3} width="100" />

                            )
                        case 4:

                            return (

                                <img src={dice4} width="100" />

                            )
                        case 5:

                            return (

                                <img src={dice5} width="100" />

                            )
                        case 6:

                            return (

                                <img src={dice6} width="100" />

                            )

                    }



                })()}
            </div>
        )
    }
}
export default Dice;